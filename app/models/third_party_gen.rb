class ThirdPartyGen < ActiveRecord::Base
  
  belongs_to :third_party_provider

  protokoll :genID, :pattern => "TP###"

end
