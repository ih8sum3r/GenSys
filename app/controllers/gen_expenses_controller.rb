class GenExpensesController < ApplicationController
  before_action :set_gen_expense, only: [:edit, :update, :destroy]

  def index
  end

  def show
    @client = ClientDetail.find(params[:id])
    date = DateTime.now
    @gen_expense = GenExpense.where("client_detail_id = ? and date between ? and ?", 
                                    params[:id], date.beginning_of_month, 
                                    date.end_of_month)
    @client_gen_set = ClientGenSet.where(client_detail_id: params[:id])

    respond_to do |format|
      format.html
      format.pdf do
        render :pdf => 'Quotation',
        :template   => 'gen_expense/bill.pdf.erb',
        :layout     => 'pdf.html.erb',
        :page_size  => 'A4',
        :dpi        => '300',
        outline: {  outline:           true,
                    outline_depth:     1 },
        margin:  {  top:               0,
                    bottom:            0,
                    left:              0,
                    right:             0},        
        :show_as_html => params[:debug].present?
      end       
    end    
  end

  def new
    @gen_expense = GenExpense.new
    @approved_client = ClientDetail.find(params[:id])
  end

  def edit
    @approved_client = ClientDetail.find(@gen_expense.client_detail_id)
  end

  def create
    @gen_expense = GenExpense.new(gen_expense_params)

    respond_to do |format|
      if @gen_expense.save
        format.html { redirect_to @gen_expense, notice: 'Gen expense was successfully created.' }
        format.json { render :show, status: :created, location: @gen_expense }
      else
        format.html { render :new }
        format.json { render json: @gen_expense.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @gen_expense.update(gen_expense_params)
        format.html { redirect_to @gen_expense, notice: 'Gen expense was successfully updated.' }
        format.json { render :show, status: :ok, location: @gen_expense, id: @gen_expense.client_detail_id }
      else
        format.html { render :edit }
        format.json { render json: @gen_expense.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @gen_expense.destroy
    respond_to do |format|
      format.html { redirect_to gen_expenses_url, notice: 'Gen expense was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private

    def set_gen_expense
      @gen_expense = GenExpense.find(params[:id])
    end

    def gen_expense_params
      params.require(:gen_expense).permit(:date, :reading, :readingBy, :batteryStatus, :radiatorStatus, :dieselCharge, :otherExpenses, :client_detail_id)
    end

      
end
